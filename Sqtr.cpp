#include <iostream>
#include <cmath>

using namespace std;

int main()
 {
     cout <<"Enter a number!" <<endl;
     int a;
     cin>> a;
     cout << a <<"^2=" << pow(a,2) << endl;
     cout << a <<"^3=" << pow(a,3) << endl;
     cout << a <<"^4=" << pow(a,4) << endl;
     cout << a <<"^5=" << pow(a,5) << endl;
     return 0;
 }
